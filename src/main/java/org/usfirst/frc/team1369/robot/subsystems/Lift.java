package org.usfirst.frc.team1369.robot.subsystems;

import org.usfirst.frc.team1369.robot.Robot;
import org.usfirst.frc.team1369.robot.RobotMap;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;

public class Lift extends Subsystem
{

	private Joystick joy;
	private Talon motor;
	//private Potentiometer angle;
	private final double DEADZONE = 0.3;

	public double INITIAL_SHOOTER_ANGLE;

	public Lift()
	{
		motor = RobotMap.MOTOR_LIFT;
		//angle = RobotMap.POTENTIOMETER;
		joy = RobotMap.JOYSTICK_SHOOTER;
		
		//REVERSING MOTOR
		motor.setInverted(false);
		motor.setSafetyEnabled(true);

		INITIAL_SHOOTER_ANGLE = (RobotMap.POTENTIOMETER.getAverageVoltage() * 10000);
	}



	public void lift()
	{
		//LIMIT

		if(RobotMap.JOYSTICK_SHOOTER.getRawButton(7))
		{
			RobotMap.LIFT_SPEED = 0.25;
		}
		if(RobotMap.JOYSTICK_SHOOTER.getRawButton(8))
		{
			RobotMap.LIFT_SPEED = 0.5;
		}
		if(RobotMap.JOYSTICK_SHOOTER.getRawButton(9))
		{
			RobotMap.LIFT_SPEED = 0.75;
		}

		//manually setting the lift angle checking voltage
		if(RobotMap.JOYSTICK_SHOOTER.getRawButton(12))
		{
			INITIAL_SHOOTER_ANGLE = (RobotMap.POTENTIOMETER.getAverageVoltage() * 10000);
		}

		double joyValue = joy.getY();
		if(joy.getMagnitude() < DEADZONE)
		{
			motor.set(0);
		}
		else
		{

			//checking for the potentiometer
			double angle = (RobotMap.POTENTIOMETER.getAverageVoltage() * 10000);

			if(Robot.magnititude(INITIAL_SHOOTER_ANGLE - angle) >= 10)
			{
				if(joyValue < -DEADZONE)
				{
					motor.set(-RobotMap.LIFT_SPEED);
				}
			}
			else
			{
				if(RobotMap.JOYSTICK_SHOOTER.getRawButton(11))
				{
					INITIAL_SHOOTER_ANGLE = (RobotMap.POTENTIOMETER.getAverageVoltage() * 10000);
					if(joyValue > DEADZONE)
					{
						motor.set(RobotMap.LIFT_SPEED);
					}
				}
				else
				{
					if(joyValue < -DEADZONE)
					{
						liftDown();
					}
					if(joyValue > DEADZONE)
					{
						liftUp();
					}
				}
			}
		}

	}

	public void forceStop()
	{
		motor.set(0);
	}

	//lift goes down
	public void liftUp()
	{
		motor.set(RobotMap.LIFT_SPEED);
	}

	//lift goes up
	public void liftDown()
	{
		motor.set(-RobotMap.LIFT_SPEED);
	}

	@Override
	protected void initDefaultCommand() 
	{
		
	}

}
