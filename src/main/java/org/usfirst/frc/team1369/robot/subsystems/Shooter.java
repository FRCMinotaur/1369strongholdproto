package org.usfirst.frc.team1369.robot.subsystems;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.usfirst.frc.team1369.robot.RobotMap;

/**
 * Created by Squidmin on 18-Jan-16.
 */

public class Shooter extends Subsystem {

    private Joystick joystick;
    private Talon leftMotor;
    private Talon rightMotor;


    public Shooter() {
        this.joystick = RobotMap.JOYSTICK_SHOOTER;
        this.leftMotor = RobotMap.MOTOR_LEFT_MOTOR_SHOOTER;
        this.rightMotor = RobotMap.MOTOR_RIGHT_MOTOR_SHOOTER;
    }

    @Override
    protected void initDefaultCommand() {

    }
    public void shoot()
    {
    	//intake
    	if(joystick.getRawButton(3))
    	{
    		leftMotor.set(-RobotMap.SHOOTER_SPEED);
            rightMotor.set(RobotMap.SHOOTER_SPEED);
    	}
    	//outtake
    	else if(joystick.getRawButton(5))
    	{
            leftMotor.set(RobotMap.SHOOTER_SPEED);
            rightMotor.set(-RobotMap.SHOOTER_SPEED);
        } 
        else 
        {
            leftMotor.set(0);
            rightMotor.set(0);
        }

    }
    public void shootCustom(double kek){
        leftMotor.set(kek);
        rightMotor.set(-kek);
    }
}
