package org.usfirst.frc.team1369.robot.commands.auto;


import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team1369.robot.Robot;
import org.usfirst.frc.team1369.robot.subsystems.DriveTrain;

/**
 * Created by Squidmin on 17-Feb-16.
 */
public class AutoEncoderDrive extends Command {

    private boolean isDone;
    private DriveTrain driveTrain;

    public AutoEncoderDrive()
    {
        driveTrain = Robot.driveTrain;
        requires(driveTrain);
    }

    @Override
    protected void initialize() {
        isDone = false;
    }

    @Override
    protected void execute() {
        driveTrain.driveDistance(0.15, 150);
        isDone = true;

    }

    @Override
    protected boolean isFinished() {
        return isDone;
    }

    @Override
    protected void end() {

    }

    @Override
    protected void interrupted() {

    }
}
