package org.usfirst.frc.team1369.robot.subsystems;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team1369.robot.Robot;
import org.usfirst.frc.team1369.robot.RobotMap;

public class DriveTrain extends Subsystem {

    //    private RobotDrive driveTrain;
    private Joystick leftStick;
    private Joystick rightStick;

    private Talon frontLeft;
    private Talon frontRight;
    private Talon backLeft;
    private Talon backRight;
    private final double DEADZONE = .2;
    private ADXRS450_Gyro gyro;
    //private Encoder encoder;
    //private Encoder encoder;

    public DriveTrain() {
        //  this.driveTrain = RobotMap.ROBOT_DRIVE;
        this.leftStick = RobotMap.JOYSTICK_LEFT;
        this.rightStick = RobotMap.JOYSTICK_RIGHT;

        //MOTORS ARE INVERTERED
        RobotMap.MOTOR_RIGHT_MOTOR_BACK.setInverted(true);
        RobotMap.MOTOR_RIGHT_MOTOR_FRONT.setInverted(true);

        /*encoder = RobotMap.MOTOR_ENCODER_LEFT;
        encoder.setMaxPeriod(1);
        encoder.setMinRate(10);
        encoder.setDistancePerPulse(5);
        encoder.setSamplesToAverage(7);*/

        this.frontRight = RobotMap.MOTOR_RIGHT_MOTOR_FRONT;
        this.frontLeft = RobotMap.MOTOR_LEFT_MOTOR_FRONT;
        this.backLeft = RobotMap.MOTOR_LEFT_MOTOR_BACK;
        this.backRight = RobotMap.MOTOR_RIGHT_MOTOR_BACK;

        this.gyro = Robot.gyro;
    }

    @Override
    protected void initDefaultCommand() {

    }

    /**
     * drive: will move the robot with tank drive
     * args: none
     */
    public void drive() {
        //motors are inverted in the DriveTrain.java subsystem file.

        //left side

        if (leftStick.getMagnitude() < DEADZONE) {
            stop();
        }
        else
        {
            if (leftStick.getY() < 0) {
                frontLeft.set(-RobotMap.DRIVE_MOTOR_SPEED);
                backLeft.set(-RobotMap.DRIVE_MOTOR_SPEED);
            } else {
                frontLeft.set(RobotMap.DRIVE_MOTOR_SPEED);
                backLeft.set(RobotMap.DRIVE_MOTOR_SPEED);
            }
        }

        if(rightStick.getMagnitude() < DEADZONE)
        {
            stop();
        }
        else
        {
            if (rightStick.getY() < 0) {
                backRight.set(-RobotMap.DRIVE_MOTOR_SPEED);
                frontRight.set(-RobotMap.DRIVE_MOTOR_SPEED);
            }
            else
            {
                backRight.set(RobotMap.DRIVE_MOTOR_SPEED);
                frontRight.set(RobotMap.DRIVE_MOTOR_SPEED);
            }
        }

        /*SmartDashboard.putString("Encoder (dist)", "" + encoder.getDistance());
        SmartDashboard.putString("Encoder (raw)", "" + encoder.get());
*/
    }

    public void driveCustom(double x, double y) {
        frontLeft.set(x);
        backLeft.set(x);
        backRight.set(y);
        frontRight.set(y);
    }

    public boolean driveDistance(double speed, double dist) {
        /*encoder.reset();
        driveCustom(speed, speed);
        while (encoder.getDistance() <= dist) {

        }
        stop();
        return true;*/
        return true;
    }

    public void stop() {
        driveCustom(0, 0);
    }

    public boolean turningByRate(double angle, double speed) {
        boolean isCompleted = false;
        long lastSysTime = System.nanoTime();
        long currSysTime = -1;
        double angleMeasured = 0;
        if (angle < 0) {
            driveCustom(speed, -speed);
            angle *= -1;
        } else {
            driveCustom(-speed, speed);
        }
        while (angleMeasured <= angle) {
            currSysTime = System.nanoTime();
            long elapsed = currSysTime - lastSysTime;
            double angleMoved = 0; //multiplys the rate by the elapsed time in secs
            if(Robot.gyro != null)
            {
                System.out.println(elapsed);
                double rate = Robot.magnititude(gyro.getRate());
                angleMoved = (rate * ((elapsed / 1000000000.0)));
            }
            angleMeasured += angleMoved;
            lastSysTime = currSysTime;
        }
        stop();
        isCompleted = true;
        return isCompleted;
    }

}
