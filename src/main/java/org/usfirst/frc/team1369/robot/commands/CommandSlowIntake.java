package org.usfirst.frc.team1369.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team1369.robot.Robot;
import org.usfirst.frc.team1369.robot.RobotMap;
import org.usfirst.frc.team1369.robot.subsystems.Shooter;

/**
 * Created by Squidmin on 19-Jan-16.
 */
public class CommandSlowIntake extends Command
{
    private Shooter shooter = Robot.shooter;
    private final double SPEED = RobotMap.INTAKE_MOTOR_SLOW_SPEED;
    
    public CommandSlowIntake()
    {
        this.requires(shooter);
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected void execute() {
        shooter.shootCustom(SPEED);
    }

    @Override
    protected boolean isFinished() {
        return false;
    }

    @Override
    protected void end() {
        shooter.shootCustom(0);
    }

    @Override
    protected void interrupted()
    {
        shooter.shootCustom(0);
    }
}
