package org.usfirst.frc.team1369.robot;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team1369.robot.camera.CameraSwitcher;
import org.usfirst.frc.team1369.robot.commands.CommandLift;
import org.usfirst.frc.team1369.robot.commands.CommandSlowIntake;
import org.usfirst.frc.team1369.robot.commands.CommandSlowOuttake;
import org.usfirst.frc.team1369.robot.commands.auto.*;
import org.usfirst.frc.team1369.robot.subsystems.*;

//another test
//test

/*
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {

	/* Variables */

    //Shooter
    public static boolean isShooterReady = true;

    //Subsystems
    public static DriveTrain driveTrain;
    public static Shooter shooter;
    public static ShooterArm shooterArm;
    public static Lift lift;
    public static Hang hang;

    //Commands
    private CommandSlowOuttake cot;
    private CommandSlowIntake inta;

    //OI (Unused)
    public static OI oi;

    //Autonomous
    private Command autonomousCommand;
    private SendableChooser chooser;

    public static ADXRS450_Gyro gyro;

    public static boolean isLifting;

    //Camera
    private CameraSwitcher cc;

    private CameraUpdater cameraUpdaterThread;

    private class CameraUpdater implements Runnable {
        private boolean shouldTerminate = false;

        public void terminate() {
            shouldTerminate = true;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    cc.run();
                    Thread.sleep(150);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (shouldTerminate) {
                    break;
                }
                System.out.println("CutForAndrewButt");
            }
        }
    }

    /*
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    public void robotInit() {
        oi = new OI();

        /*cs1 = CameraServer.getInstance();
        cs1.setQuality(20);
        cs1.startAutomaticCapture("cam0");
        */

        isLifting = false;

        //Gyro Initialization
        gyro = new ADXRS450_Gyro();

        //Subsystem Initialization
        driveTrain = new DriveTrain();
        shooter = new Shooter();
        shooterArm = new ShooterArm();
        lift = new Lift();
        hang = new Hang();

        //Command Initialization
        cot = new CommandSlowOuttake();
        inta = new CommandSlowIntake();

        //Chooser Initialization
        chooser = new SendableChooser();
        chooser.addDefault("Do Nothing", null);
        chooser.addObject("Debug", new AutoDebug());
        chooser.addObject("DriveStraight", new AutoDriveStraight());
        chooser.addObject("DriveTest", new AutoTest());
        chooser.addObject("Driving", new AutoDriving());
        chooser.addObject("EncoderDrive", new AutoEncoderDrive());
        chooser.addObject("GyroTurn", new AutoGyroTurn());
        SmartDashboard.putData("Auto Chooser", chooser);

    }

    public static double magnititude(double in)
    {
        double out = in;
        if(out < 0)
        {
            out *= -1;
        }
        return out;
    }

    /*
     * This function is called once each time the robot enters Disabled mode.
     * You can use it to reset any subsystem information you want to clear when
     * the robot is disabled.
     */
    public void disabledInit()
    {
        if(cc != null)
        {
            cc.end();
        }
    }

    public void disabledPeriodic()
    {
        Scheduler.getInstance().run();

    }

    /*
     * This autonomous (along with the chooser code above) shows how to select between different autonomous modes
     * using the dashboard. The sendable chooser code works with the Java SmartDashboard. If you prefer the LabVIEW
     * Dashboard, remove all of the chooser code and uncomment the getString code to get the auto name from the text box
     * below the Gyro
     *
     * You can add additional auto modes by adding additional commands to the chooser code above (like the commented example)
     * or additional comparisons to the switch structure below with additional strings & commands.
     */
    public void autonomousInit() {
        try {
            if (chooser.getSelected() != null) {
                autonomousCommand = (Command) chooser.getSelected();
                autonomousCommand.start();
            } else {
                System.out.println("No Autonomous Mode Selected");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * This function is called periodically during autonomous
     */
    public void autonomousPeriodic() {
        Scheduler.getInstance().run();
    }

    public void teleopInit() {
        if (autonomousCommand != null) autonomousCommand.cancel();

        /**if(cc == null)
        {
            cc = new CameraSwitcher();
        }
        if(cc != null)
        {
            cc.init();
        }*/
    }


    /*
     * This function is called periodically during operator control
     */
    public void teleopPeriodic() {
        Scheduler.getInstance().run();



        //camera code
        try{
            //cc.run();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }


        try {


            if(!isLifting) {
                //Joystick: Left & Right
                //DriveTrain Move
                //RobotMap.DRIVE_MOTOR_SPEED = ((.3) * (RobotMap.JOYSTICK_RIGHT.getThrottle())) + (4.16667);
                RobotMap.DRIVE_MOTOR_SPEED = (0.4 * RobotMap.JOYSTICK_RIGHT.getZ()) + .6;
                SmartDashboard.putString("TEST:", "" + RobotMap.JOYSTICK_RIGHT.getZ());
                driveTrain.drive();

                if (RobotMap.JOYSTICK_LEFT.getRawButton(10)) {
                    //RobotMap.MOTOR_ENCODER_LEFT.reset();
                }

                //Joystick: Shooter
                lift.lift();
                hang.hang();

                if (RobotMap.JOYSTICK_SHOOTER.getRawButton(10))
                {
                    new CommandLift().start();
                    isLifting = true;
                }

                if (RobotMap.JOYSTICK_SHOOTER.getThrottle() < .6) {
                    RobotMap.SHOOTER_SPEED = ((((-RobotMap.JOYSTICK_SHOOTER.getThrottle()) + 1.0) / 2.0));
                } else {
                    RobotMap.SHOOTER_SPEED = .2;
                }

                if (RobotMap.JOYSTICK_SHOOTER.getRawButton(6)) {
                    cot.start();
                } else {
                    cot.cancel();
                }

                if (RobotMap.JOYSTICK_SHOOTER.getRawButton(4)) {
                    inta.start();
                } else {
                    inta.cancel();
                }

                if (!cot.isRunning() && !inta.isRunning()) {
                    shooter.shoot();
                }

                if (RobotMap.JOYSTICK_SHOOTER.getTrigger() && isShooterReady) {
                    shooterArm.activate();
                    isShooterReady = false;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        if (!isOperatorControl()) {

        }
    }

    /*
     * This function is called periodically during test mode
     */
    public void testPeriodic() {
        //   LiveWindow.run();
        System.out.println(gyro.getAngle());

    }

}
